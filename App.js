import React from 'react';

import MyStack from "./navigation/Navigation";

class App extends React.Component {
    render() {
        return (
            <MyStack/>
        );
    }
}

export default App;


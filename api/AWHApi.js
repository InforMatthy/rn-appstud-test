const URL = "https://afternoon-waters-49321.herokuapp.com";

/**
 * Call api for get all playlist
 * @returns {Promise<any | void>}
 */
export function getAllEditors() {
    const url = URL + '/v1/browse/featured-playlists';
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.error(error));
}

/**
 * Call api for get playlist by id
 * @param idPlaylist
 * @returns {Promise<any | void>}
 */
export function getPlaylistById(idPlaylist) {
    const url = URL + '/v1/playlists/' + idPlaylist;
    return fetch(url)
        .then((response) => response.json())
        .catch((error) => console.error(error));
}
import React from 'react'
import { StyleSheet, TouchableOpacity, Image } from 'react-native';

/**
 * Display Item for EditorsPicks Screen
 */
class EditorItem extends React.Component {

    render() {
        const { editor, displayPlaylistDetail } = this.props;
        const image = editor.images[0].url;
        return (
            <TouchableOpacity
                style={styles.main_container}
                onPress={() => displayPlaylistDetail(editor.id)}
            >
                <Image
                    source={{uri: image}}
                    style={styles.image} />
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        marginTop: 5
    },
    image: {
        height: 170,
        resizeMode: 'contain',
        marginBottom: 5
    }
});

export default EditorItem;
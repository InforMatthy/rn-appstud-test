import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

/**
 * Item for playlist detail, each represent one song
 */
class PlaylistItem extends React.Component {

    _displayItem() {
        const { music } = this.props;
        let styleTitle, styleArtists;
        if (music.track) {
            if (music.track.preview_url) {
                styleTitle = {
                    color: 'white',
                    fontWeight: 'bold'
                };
                styleArtists = {
                    color: '#a6a6a6'
                };
            } else {
                styleTitle = {
                    color: '#666666',
                    fontWeight: 'bold'
                };
                styleArtists = {
                    color: '#666666'
                };
            }
            return (
                <View style={styles.main_container}>
                    <Text style={styleTitle}>
                        {music.track.name}
                    </Text>
                    <Text style={styleArtists}>
                        {music.track.artists[0].name}
                    </Text>
                </View>
            )
        } else {
            console.log('Not existing');
        }
    }

    render() {
        return (
            <View>
                {this._displayItem()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        marginBottom: 25,
        marginLeft: 15
    }
});

export default PlaylistItem;
import React from 'react';
import {StyleSheet, View, Text, ActivityIndicator, Image, FlatList} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { getPlaylistById } from '../api/AWHApi';
import numeral from 'numeral';

import PlaylistItem from "../components/PlaylistItem";

/**
 * Use for display new Screen for playlist detail
 */
export default class PlaylistDetail extends React.Component {

    constructor(props) {
        super(props);
        this.idPlaylist = this.props.route.params.idPlaylist;
        this.state = {
            playlistDetail: undefined,
            isLoading: true
        }
    }

    componentDidMount() {
        getPlaylistById(this.idPlaylist).then(data => {
            this.setState({
                playlistDetail: data,
                isLoading: false
            });
            console.log("Loading Playlist detail completed !");
        });
    }

    /**
     * Display a little loader when the api download
     * @returns {*}
     * @private
     */
    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    _displayPlaylistDetail() {
        const { playlistDetail } = this.state;
        if (playlistDetail) {
            // Use numeral for display magnify number
            const followers = numeral(playlistDetail.followers.total).format('0.0a');
            return (
                <View>
                    <LinearGradient colors={['black', 'rgba(0, 153, 51, 0.5)']} style={styles.linear_gradient}>
                        <View style={styles.header_container}>
                            <View style={styles.image_container}>
                                <Image
                                    source={{uri: playlistDetail.images[0].url}}
                                    style={styles.image} />
                            </View>
                            <View style={styles.header_aside_container}>
                                <Text style={styles.title}>
                                    {playlistDetail.name}
                                </Text>
                                <Text style={styles.subtitle}>
                                    Playlist by Spotify
                                </Text>
                                <Text style={styles.description}>
                                    {playlistDetail.description}
                                </Text>
                                <Text style={styles.followers}>
                                    {followers.toUpperCase()} followers
                                </Text>
                            </View>
                        </View>
                    </LinearGradient>
                    <FlatList style={styles.playlist_container}
                              data={playlistDetail.tracks.items}
                              keyExtractor={(item, index) => index.toString()}
                              renderItem={({item}) => <PlaylistItem music={item} />}
                    />
                </View>
            );
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'black'}}>
                {this._displayLoading()}
                {this._displayPlaylistDetail()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    linear_gradient: {
        paddingTop: 50,
        paddingBottom: 20,
        borderBottomWidth: 2,
        borderBottomColor: '#00b359'
    },
    header_container: {
        flexDirection: 'row',
        paddingRight: 15,
        paddingLeft: 15
    },
    image_container: {
        flex: 1
    },
    image: {
        height: 170,
        resizeMode: 'contain',
        borderRadius: 5
    },
    header_aside_container: {
        flex: 1,
        marginLeft: 10
    },
    title: {
        color: 'white',
        fontSize: 26,
        fontWeight: 'bold'
    },
    subtitle: {
        color: '#a6a6a6',
        marginBottom: 20
    },
    description: {
        color: 'white'
    },
    followers: {
        color: '#a6a6a6',
    },
    playlist_container: {
        marginTop: 10
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
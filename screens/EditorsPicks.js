import * as React from 'react';
import { View, Text, FlatList, StyleSheet, ActivityIndicator } from 'react-native';

import EditorItem from "../components/EditorItem";
import { getAllEditors } from "../api/AWHApi";

/**
 * MAIN VIEWS
 */
class EditorsPicks extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            editors: [],
            isLoading: false
        };
    }

    componentDidMount() {
        this._loadEditors();
    }

    /**
     * Use for display playlist detail, and move to new screen
     * @param idPlaylist
     * @private
     */
    _displayPlaylistDetail = (idPlaylist) => {
        console.log("Display playlist with id " + idPlaylist);
        this.props.navigation.navigate("PlaylistDetail", { idPlaylist: idPlaylist });
    };

    /**
     * Go call the api for download all data for display
     * @private
     */
    _loadEditors() {
        this.setState({ isLoading: true });
        getAllEditors().then(data => {
            this.setState({
                editors: data.playlists.items,
                isLoading: false
            });
            console.log("Loading Editors picks completed !");
        });
    }

    /**
     * Display a little loader when the api download
     * @returns {*}
     * @private
     */
    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    render() {
        return (
            <View style={styles.main_container}>
                <FlatList
                    data={this.state.editors}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({item}) => <EditorItem editor={item} displayPlaylistDetail={this._displayPlaylistDetail} />}
                    numColumns={2}
                />
                {this._displayLoading()}
            </View>
        );
    }
}

export default EditorsPicks;


const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        backgroundColor: 'black'
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
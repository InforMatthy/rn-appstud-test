import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import EditorsPicks from "../screens/EditorsPicks";
import PlaylistDetail from "../screens/PlaylistDetail";

const Stack = createStackNavigator();

class MyStack extends React.Component {

    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator
                    screenOptions={{
                        headerStyle: {
                            backgroundColor: 'black'
                        },
                        headerTintColor: 'white',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },
                    }}
                >
                    <Stack.Screen
                        name="Home"
                        component={EditorsPicks}
                        options={{
                            title: 'Editor\'s picks',
                        }}
                    />
                    <Stack.Screen
                        name="PlaylistDetail"
                        component={PlaylistDetail}
                        options={{
                            headerShown: false
                        }}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}

export default MyStack;